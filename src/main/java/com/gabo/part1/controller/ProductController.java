package com.gabo.part1.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import com.gabo.part1.model.Product;
import com.gabo.part1.service.ProductService;
import com.google.common.net.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/v1/products")
@Api(value = "Desafio 1 - API")
public class ProductController {
    @Autowired
    private ProductService productService;

    /**
     * Busca todos os produtos salvos no banco.
     * 
     * @return Retorna o status da busca.
     */
    @GetMapping
    @ApiOperation(value = "Busca todos os produtos")
    public ResponseEntity<?> findAll() {
        return new ResponseEntity<List<Product>>(productService.findAll(), HttpStatus.OK);
    }

    /**
     * Busca um produto pelo seu id.
     * 
     * @param id
     * @return Retorna o status da busca.
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "Busca um produto pelo id")
    public ResponseEntity<?> findById(@PathVariable int id) {
        Optional<Product> productOpt = productService.findById(id);

        if (productOpt.isPresent()) {
            return new ResponseEntity<Product>(productOpt.get(), HttpStatus.OK);
        }
        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

    /**
     * Adiciona um novo produto ao banco, caso o mesmo produto tenha sido adicionado
     * recentement retorna 403.
     * 
     * @param product
     * @return Retorna status da busca.
     */
    @PostMapping(consumes = "application/json", produces = "application/json")
    @ApiOperation( value = "Adiciona um produto novo")
    public ResponseEntity<?> add(@RequestBody @Valid Product product) {
        List<Product> recentProduct = productService.findAllRecent();
        for (Product p : recentProduct) {
            if (p.getName().equals(product.getName()) && p.getId() == product.getId()) {
                return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
            }
        }
        productService.add(product);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    /**
     * Realiza o update de um produto.
     * 
     * @param product
     * @return Retorna status do update.
     */
    @PutMapping("update/{id}")
    @ApiOperation( value = "Realiza o update de um produto")
    public ResponseEntity<?> update(@RequestBody Product product) {
        Optional<Product> optProduct = productService.update(product);
        if (optProduct.isPresent()) {
            return new ResponseEntity<>(optProduct.get(), HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

    /**
     * Deleta um produto.
     * 
     * @param id
     * @return Retorna status do delete.
     */
    @DeleteMapping("/delete/{id}")
    @ApiOperation( value = "Deleta um produto")
    public ResponseEntity<?> delete(@PathVariable int id) {
        Optional<Product> optProduct = productService.delete(id);
        if (optProduct.isPresent()) {
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }
}