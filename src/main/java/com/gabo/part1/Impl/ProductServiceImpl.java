package com.gabo.part1.Impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.gabo.part1.model.Product;
import com.gabo.part1.repository.ProductRepository;
import com.gabo.part1.service.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * Implantação da interface de serviço do produto.
 */
@Service
public class ProductServiceImpl implements ProductService {
    public static List<Product> productList = new ArrayList<>();

    @Autowired
    private ProductRepository productRepository;
    /**
     * Busca no banco todos os produtos cadastrados
     */
    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }
    /**
     * Busca no banco produtos por id.
     */
    @Override
    public Optional<Product> findById(int id) {
        return productRepository.findById(id);
    }

    /**
     * Adiciona um produto no banco de dados.
     */
    @Override
    public void add(Product product) {
       product.setDate(LocalDateTime.now());
       productRepository.save(product);
    }

    /**
     * Deleta um produto do banco de dados.
     */
    @Override
    public Optional<Product> delete(int id) {
        Optional<Product> productOptional = productRepository.findById(id);

        if (productOptional.isPresent()) {
            productRepository.delete(productOptional.get());
            return productOptional;
        }

        return Optional.empty();
    }

    /**
     * Faz o update de um produto no banco de dados.
     */
    @Override
    public Optional<Product> update(Product product) {
        Optional<Product> productOptional = productRepository.findById(product.getId());

        if (productOptional.isPresent()) {
            Product selectedProduct = productOptional.get();
            //Verifica se o nome foi alterado
            if (product.getName() != null) {
                selectedProduct.setName(product.getName());
            }
            //Salva o produto
           productRepository.save(selectedProduct);

            return Optional.of(selectedProduct);
        }

        return Optional.empty();
    }

    /**
     * Busca no banco de dados todos os produtos adicionados nos útimos 10 minutos.
     */
    @Override
    public List<Product> findAllRecent() {
        return productRepository.findAllRecent();
    }

    

}