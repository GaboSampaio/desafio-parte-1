package com.gabo.part1.service;

import java.util.List;
import java.util.Optional;

import com.gabo.part1.model.Product;

import org.springframework.stereotype.Service;
/**
 * Interface serviço do produto.
 */
@Service
public interface ProductService {
    public List<Product> findAll();

    public Optional<Product> findById(int id);

    public void add(Product product);

    public Optional<Product> delete(int id);

    public Optional<Product> update(Product product);

    public List<Product> findAllRecent();
}
