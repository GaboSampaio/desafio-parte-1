package com.gabo.part1.repository;

import java.util.List;

import com.gabo.part1.model.Product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
/** Interface responsável pela interação com o banco de dados */
@Repository
public interface ProductRepository extends JpaRepository<Product,Integer>{
    //Seleciona todos os produtos que foram adicionados nos últimos 10 minutos.
    @Query(value ="SELECT * FROM products WHERE date >= now() - INTERVAL 10 MINUTE", nativeQuery = true)
    List<Product> findAllRecent();
}