package com.gabo.part1.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
/**
 * Classe que descreve um produto.
 */
@Entity
@Table(name = "products")
public class Product {
    @Id
    private int id;
    @NotNull
    private String name;
    @Column
    private LocalDateTime date;

    public Product() {
        super();
    }

    public Product(final int id, final String name, LocalDateTime date) {
        super();
        this.id = id;
        this.name = name;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setDate(final LocalDateTime date) {
        this.date = date;
    }
    
}
