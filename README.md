Desafio 1 

Projeto API REST.

## Tecnologias
  - Java 1.8
  - Spring Boot
  - Swagger
  - H2 Mysql Database
  - JUnit 4
  

## Requisitos
- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)


## Uso

Para executar o projeto você deve executar o comando na pasta raiz que contem o arquivo pom.xml:

```shell
mvn spring-boot:run
```


## Uso da API
A api pode ser utilizada com requests http.
Uma vez que a aplicação estiver rodando acesse
[Swagger](http://localhost:8080/swagger-ui.html) para verificar os comandos da api.

